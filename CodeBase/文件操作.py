#文件操作步骤
#1. 打开⽂件
#2. 读写等操作
#3. 关闭⽂件

# 1. 打开⽂件
f = open('test.txt', 'w')
# 2.⽂件写⼊
f.write('hello world')
# 3. 关闭⽂件
f.close()

#readlines:readlines可以按照⾏的⽅式把整个⽂件中的内容进⾏⼀次性读取，并且返回的是⼀个列表，其中每⼀⾏的数据为⼀个元素。
f = open('test.txt')
content = f.readlines()
# ['hello world\n', 'abcdefg\n', 'aaa\n', 'bbb\n', 'ccc']
print(content)
# 关闭⽂件
f.close()

#readline()⼀次读取⼀⾏内容。
f = open('test.txt')
content = f.readline()
print(f'第⼀⾏：{content}')
content = f.readline()
print(f'第⼆⾏：{content}')
# 关闭⽂件
f.close()

#需求：批量修改⽂件名，既可添加指定字符串，⼜能删除指定字符串。
# 步骤
# 1. 设置添加删除字符串的的标识
# 2. 获取指定⽬录的所有⽂件
# 3. 将原有⽂件名添加/删除指定字符串，构造新名字
# 4. os.rename()重命名
import os

# 设置重命名标识：如果为1则添加指定字符，flag取值为2则删除指定字符
flag = 1
# 获取指定⽬录
dir_name = './'
# 获取指定⽬录的⽂件列表
file_list = os.listdir(dir_name)
# print(file_list)
# 遍历⽂件列表内的⽂件
for name in file_list:
    # 添加指定字符
    if flag == 1:
        new_name = 'Python-' + name
    # 删除指定字符
    elif flag == 2:
        num = len('Python-')
        new_name = name[num:]
    # 打印新⽂件名，测试程序正确性
    print(new_name)

    # 重命名
    os.rename(dir_name + name, dir_name + new_name)
#1.元组定义：单个数据的元组数据后要加,
t1=(11,12,13)
t2=(10)
t3=(10,)
print(type(t1))
print(type(t2))
print(type(t3))
#2.元组不支持修改，只支持查找
tuple1 = ('aa', 'bb', 'cc', 'bb')
print(tuple1[0])
print(tuple1.count('bb'))
print(len(tuple1))
#3.元组里有列表的话，可以修改列表里的数据
tuple2 = (10, 20, ['aa', 'bb', 'cc'], 50, 30)
print(tuple2[2])
tuple2[2][0]= 'ssss'
print(tuple2)

#1、打印小猫爱吃鱼，小猫要喝水
#	定义猫类

#定义类为猫
class Cat():
    def __init__(self):
        self.food='fish'
        self.drink='water'
    def __str__(self):
        return f'小猫爱吃{self.food},小猫要喝{self.drink}'
huahua=Cat()
print(huahua)

# 2、小明爱跑步，爱吃东西。
#     1）小明体重75.0公斤
#     2）每次跑步会减肥0.5公斤
#     3）每次吃东西体重会增加1公斤
#     4）小美的体重是45.0公斤

#定义类
class renlei():
    def __init__(self,name,weight):
        #定义属性体重weight，名字name
        self.name=name
        self.weight=weight
    #定义方法，跑步一次体重减0.5，吃东西一次体重加1
    def pao(self,n):
        self.weight-=0.5*n
    def chi(self,m):
        self.weight+=m
    #魔法方法
    def __str__(self):
        return f'{self.name}的体重是{self.weight}kg'
if __name__== '__main__':
    user1=renlei('小明',75)
    user1.pao(4)
    user1.chi(1)
    print(user1)

    user2=renlei('小美',45)
    user2.pao(4)
    user2.chi(1)
    print(user2)


# 3、摆放家具
#     需求：
#     1）.房子有户型，总面积和家具名称列表
#        新房子没有任何的家具
#     2）.家具有名字和占地面积，其中
#        床：占4平米
#        衣柜：占2平面
#        餐桌：占1.5平米
#     3）.将以上三件家具添加到房子中
#     4）.打印房子时，要求输出:户型，总面积，剩余面积，家具名称列表

#家具类
class jiaju():
    def __init__(self,name,area):
        self.name=name
        self.area=area
#房子类
class room():
    def __init__(self,huxing,area):
        self.huxing=huxing
        self.area=area
        self.free_area=area
        self.jiaju=[]
    #魔法方法
    def __str__(self):
        return f'房子户型是{self.huxing},面积为{self.area},剩余面积为{self.free_area},屋内家具有{self.jiaju}'
    def add_jiaju(self,item):
        #放家具
        if self.free_area>=item.area:
            self.jiaju.append(item.name)
            self.free_area-=item.area
        else:
            print('放不下')
bed=jiaju('床',4)
yigui=jiaju('衣柜',2)
canzhuo=jiaju('餐桌',1.5)
qiuchang=jiaju('球场',2000)
room1=room('开间',40)
print(room1)
room1.add_jiaju(bed)
print(room1)
room1.add_jiaju(yigui)
print(room1)
room1.add_jiaju(canzhuo)
print(room1)
room1.add_jiaju(qiuchang)
print(room1)


# 4.士兵开枪
#     需求：
#     1）.士兵瑞恩有一把AK47
#     2）.士兵可以开火(士兵开火扣动的是扳机)
#     3）.枪 能够 发射子弹(把子弹发射出去)
#     4）.枪 能够 装填子弹 --增加子弹的数量

#枪类
class gun():
    #属性：型号，子弹数
    #方法：发射子弹，装填子弹
    def __init__(self,model,bullet_count):
        self.model=model
        self.bullet_count=bullet_count
        self.max_bullet_count=20
    #发射方法，有子弹时，每发射一次，减少一颗子弹
    def shoot(self):
        if self.bullet_count>0:
            self.bullet_count-=1
        else:
            print('没有子弹了')
    #装填子弹方法，子弹未装满时，装满子弹
    def add_bullet(self):
        if self.bullet_count<self.max_bullet_count:
            self.bullet_count+=(self.max_bullet_count-self.bullet_count)
#士兵类
class soldier():
    #属性，name,枪名gun_name
    #方法,开火fire
    def __init__(self,xingming,gun_name):
        self.xingming=xingming
        self.gun_name=gun_name
    #开火方法，调用gun里面的发射方法
    def fire(self):
        self.gun_name.shoot()
        #装子弹
    def add_bullets(self):
        self.gun_name.add_bullet()
gun1=gun('AK47',10)
soldier1=soldier('ruien',gun1)
soldier1.fire()
soldier1.fire()
soldier1.fire()
print(f'士兵{soldier1.xingming}射击3次后剩余子弹数为{soldier1.gun_name.bullet_count}')
soldier1.add_bullets()
print(f'士兵{soldier1.xingming}射击3次后，将子弹装满后，剩余子弹数为{soldier1.gun_name.bullet_count}')




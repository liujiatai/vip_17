#1.数据类型举例
a=1
print(type(a))
b=1.1
print(type(b))
c=True
print(type(c))
d='12345'
print(type(d))
e=[1,2,3,4]
print(type(e))
f=(1,2,3,5)
print(type(f))
h={4,5,6,7}
print(type(h))
g={'name':'lilei','age':18}
print(type(g))

#2.输出
age=18
name = 'xiaoming'
weight = 63.4
student_id = 5
#我的名字是xiaoming
print('我的名字是%s' % name)
#我的学号是0005
print('我的学号是%05d' % student_id)
#我的体重是63.40公斤
print('我的体重是%.2f' % weight)
#我的名字是lilei, 明年19岁
print('我的名字是%s,明年%d岁'% (name,age+1))
print(f'我的名字是{name}，明年{age+1}岁')

#3,输入
# password = input('请输入密码')
# print(f'您输入的密码是{password}')

#4.字符串
stra = "hello world and superctest and chaoge and Python"
print(stra.find('and'))
print(stra.find('and',15,30))
print(stra.index('and',15,30))
print(stra.find('ands',15,30))
print(stra.count('and'))
print(stra.replace('and', 'he'))
print(stra)
print(stra.split('and'))
print(stra.split(' '))
print(stra.split('and',2))

#5.join
list1 = ['chao', 'ge', 'test', 'promotion']
print(','.join(list1))

#6.上公交找座位
"""
1. 如果有钱，则可以上⻋
 2. 上⻋后，如果有空座，可以坐下
 上⻋后，如果没有空座，则站着等空座位
如果没钱，不能上⻋
"""
money =1
seat = 0
if money == 1:
    print('余额充足，可以上车')
    if seat ==1:
        print('有座位，可以坐')
    else:
        print('没有座位')
else:
    print('余额不足，请充值')

#7.猜拳游戏
"""
提示：0-⽯头，1-剪⼑，2-布
1. 出拳
玩家输⼊出拳
电脑随机出拳
2. 判断输赢
玩家获胜
平局
电脑获胜
"""
#导入random模块
import random
#电脑出拳的随机数字
computer = random.randint(0,2)
print(computer)
player = int(input('请出拳：0-⽯头，1-剪⼑，2-布'))
#玩家赢
if ((player == 0) and (computer == 1)) or ((player == 1) and (computer == 2)) or ((player == 2) and (computer == 0)):
    print('赢了')
#平局
elif player == computer:
    print('平局')
#玩家输
else:
    print('输了')

#8. while练习
# 输出10个*
# i=0
# while i<11:
#     print('*')
#     i+=1

#9.while 练习2,求100以内累加和
# i=0
# n=0
# while i<=100:
#     n+=i
#     i+=1
# print(n)

#10.while练习，求100以内偶数累加和
# i=1
# add=0
# while i<=100:
#     if i%2 == 0:
#         add+=i
#     i+=1
# print(add)

#11.while条件下break和continue
#break
# i=0
# while i<=5:
#     if i == 4:
#         print('吃饱了')
#         break
#     i+=1
#     print(f'吃了{i}个苹果')
#continue
# i=0
# while i<=5:
#     if i == 3:
#         print('遇到虫子跳过')
#         i+=1
#         continue
#     print(f'吃了{i}个苹果')
#     i+=1

#12.while嵌套，用*打印方形
# i=0
# while i<=5:
#     n=0
#     while n<=5:
#         print('*',end="")
#         n+=1
#     print()
#     i+=1

#13.打印行数和*数相等的三角形
# i=0
# while i<=5:
#     n=0
#     while n<=i:
#        print('*',end='')
#        n+=1
#     print()
#     i+=1

#14.打印乘法表
# i=1
# while i<=9:
#     n=1
#     while n<=i:
#         print(f'{i}*{n}={i*n}',end="\t")
#         n+=1
#     print()
#     i+=1

#15.打印三角形
#正三角形
n=5
for i in range(n+1):
    for j in range(0, (n+1) - i):
        print(end=" ")
    for k in range((n+1) - i, (n+1)):
        print("*", end=" ")

    print("")
#倒等边三角形
n=5
for i in range(n):
    for j in range(0, i):
        print(end=" ")
    for k in range(i, n):
        print("*", end=" ")

    print("")
#左上角实心三角形
n=5
for i in range (0,n+1):
    for j in range (0,n-i):
         print("*",end=" ")
    print()
#左上角空心三角形
n=5
for i in range (0,n+1):
    for j in range (0,n-i):
      if (i==0) or (j==0) or i+j+1==n:
         print("*",end=" ")
      else:
          print(" ",end=" ")
    print()

#右上角实心三角形
n=5
for i in range(n):
    for j in range(0,i):
        print(" ", end=" ")
    for k in range(i,n):
        print("*", end=" ")
    print("")
#右上角空心三角形
n=5
for i in range (1,n+1):
    for j in range (1,n+1):
        if i==1 or j==n or i==j:
            print("*",end=" ")
        else:
            print(" ",end=" ")
    print()


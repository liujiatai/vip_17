#1.洗衣机类、对象
class Washer():
    def wash(self):
        print('我会洗⾐服')
        print(self)
# 2. 创建对象
haier1 = Washer()
print(haier1)
# haier1对象调⽤实例⽅法
haier1.wash()
haier2 = Washer()
print(haier2)

haier1.width = 500
haier1.height = 800
print(f'haier1洗⾐机的宽度是{haier1.width}')
print(f'haier1洗⾐机的⾼度是{haier1.height}')
# 3 类⾥⾯获取对象属性
# 定义类
class Washer():
    def print_info(self):
        # 类⾥⾯获取实例属性
        print(f'haier1洗⾐机的宽度是{self.width}')
        print(f'haier1洗⾐机的⾼度是{self.height}')
# 创建对象
haier1 = Washer()
# 添加实例属性
haier1.width = 500
haier1.height = 800
haier1.print_info()

# 4.魔法方法__init__() ⽅法的作⽤：初始化对象。
class Washer():

    # 定义初始化功能的函数
    def __init__(self):
        # 添加实例属性
        self.width = 500
        self.height = 800

    def print_info(self):

        # 类⾥⾯调⽤实例属性
        print(f'洗⾐机的宽度是{self.width}, ⾼度是{self.height}')

haier1 = Washer()
haier1.print_info()
# 4.1.2 带参数的__init__()
class Washer():
    def __init__(self, width, height):
        self.width = width
        self.height = height
    def print_info(self):
        print(f'洗⾐机的宽度是{self.width}')
        print(f'洗⾐机的⾼度是{self.height}')
haier1 = Washer(10, 20)
haier1.print_info()
haier2 = Washer(30, 40)
haier2.print_info()
# 4.2 __str__()
class Washer():
    def __init__(self, width, height):
        self.width = width
        self.height = height
    def __str__(self):
        return '这是海尔洗⾐机的说明书'
haier1 = Washer(10, 20)
# 这是海尔洗⾐机的说明书
print(haier1)
# 4.3 __del__()
class Washer():
    def __init__(self, width, height):
        self.width = width
        self.height = height
    def __del__(self):
        print(f'{self}对象已经被删除')
haier1 = Washer(10, 20)
del haier1

# 5.1.1 需求
# 需求主线：
# 1. 被烤的时间和对应的地⽠状态：
# 0-3分钟：⽣的
# 3-5分钟：半⽣不熟
# 5-8分钟：熟的
# 超过8分钟：烤糊了
# 2. 添加的调料：
# ⽤户可以按⾃⼰的意愿添加调料
# 5.1.2 步骤分析
# 需求涉及⼀个事物： 地⽠，故案例涉及⼀个类：地⽠类。
# 5.1.2.1 定义类
# 地⽠的属性
# 被烤的时间
# 地⽠的状态
# 添加的调料
# 地⽠的⽅法
# 被烤
# ⽤户根据意愿设定每次烤地⽠的时间
# 判断地⽠被烤的总时间是在哪个区间，修改地⽠状态
# 添加调料
# ⽤户根据意愿设定添加的调料
# 将⽤户添加的调料存储
# 显示对象信息
# 5.1.2.2 创建对象，调⽤相关实例⽅法
class SweetPotato():
    def __init__(self):
        # 被烤的时间
        self.cook_time = 0
        # 地⽠的状态
        self.cook_static = '⽣的'
        # 调料列表
        self.condiments = []
# 5.1.3.2 定义烤地⽠⽅法
    def cook(self, time):
        """烤地⽠的⽅法"""
        self.cook_time += time
        if 0 <= self.cook_time < 3:
            self.cook_static = '⽣的'
        elif 3 <= self.cook_time < 5:
            self.cook_static = '半⽣不熟'
        elif 5 <= self.cook_time < 8:
            self.cook_static = '熟了'
        elif self.cook_time >= 8:
            self.cook_static = '烤糊了'
# 5.1.3.3 书写str魔法⽅法，⽤于输出对象状态
    def __str__(self):
        return f'这个地⽠烤了{self.cook_time}分钟, 状态是{self.cook_static}'
# 5.1.3.4 创建对象，测试实例属性和实例⽅法
digua1 = SweetPotato()
print(digua1)
digua1.cook(2)
print(digua1)
# 5.1.3.5 定义添加调料⽅法，并调⽤该实例⽅法
    def add_condiments(self, condiment):
        """添加调料"""
        self.condiments.append(condiment)
    def __str__(self):
        return f'这个地⽠烤了{self.cook_time}分钟, 状态是{self.cook_static}, 添加的调料有{self.condiments}'

digua1 = SweetPotato()
print(digua1)
digua1.cook(2)
digua1.add_condiments('酱油')
print(digua1)
digua1.cook(2)
digua1.add_condiments('辣椒⾯⼉')
print(digua1)
digua1.cook(2)
print(digua1)
digua1.cook(2)
print(digua1)

# 6、摆放家具
#     需求：
#     1）.房子有户型，总面积和家具名称列表
#        新房子没有任何的家具
#     2）.家具有名字和占地面积，其中
#        床：占4平米
#        衣柜：占2平面
#        餐桌：占1.5平米
#     3）.将以上三件家具添加到房子中
#     4）.打印房子时，要求输出:户型，总面积，剩余面积，家具名称列表
#定义家具类
class jiaju():
    #家具的属性
    def __init__(self,name,area):
        self.name = name
        self.area = area
#定义房子类
class house():
    #房子的属性
    def __init__(self,huxing,area):
        self.huxing = huxing
        self.area = area
        self.free_area = area
        self.jiaju = []
    #定义返回魔法方法
    def __str__(self):
        return f'房子的户型是{self.huxing},房子的面积是{self.area},屋里有家具{self.jiaju},剩余面积还有{self.free_area}'
    #定义方法：添加家具
    def add_jiaju(self,item):
        if self.free_area >= item.area:
            self.jiaju.append(item.name)
            self.free_area -= item.area
        else:
            print('放不下')
if __name__=='__main__':
    bed = jiaju('床',4)
    yigui = jiaju('衣柜',2)
    canzhuo = jiaju('餐桌',1.5)
    room = house('开间',40)
    print(room)
    room.add_jiaju(bed)
    print(room)
    room.add_jiaju(yigui)
    print(room)
    room.add_jiaju(canzhuo)
    print(room)

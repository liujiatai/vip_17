#1.需求：进⼊系统显示系统功能界⾯，功能如下：
# 1、添加学员
# 2、删除学员
# 3、修改学员信息
# 4、查询学员信息
# 5、显示所有学员信息
# 6、退出系统
# 系统共6个功能，⽤户根据⾃⼰需求选取。
# 1.2步骤分析
# 1. 显示功能界⾯
# 2. ⽤户输⼊功能序号
# 3. 根据⽤户输⼊的功能序号，执⾏不同的功能(函数)
# 3.1 定义函数
# 3.2 调⽤函数
# 1.3.1 显示功能界⾯
# 定义函数 print_info ，负责显示系统功能。
def print_info():
    print('-' * 20)
    print('欢迎登录学员管理系统')
    print('1: 添加学员')
    print('2: 删除学员')
    print('3: 修改学员信息')
    print('4: 查询学员信息')
    print('5: 显示所有学员信息')
    print('6: 退出系统')
    print('-' * 20)


print_info()
# 1.3.2 ⽤户输⼊序号，选择功能
user_num = input('请选择您需要的功能序号：')
# 1.3.3 根据⽤户选择，执⾏不同的功能
if user_num == '1':
 print('添加学员')
elif user_num == '2':
 print('删除学员')
elif user_num == '3':
 print('修改学员信息')
elif user_num == '4':
 print('查询学员信息')
elif user_num == '5':
 print('显示所有学员信息')
elif user_num == '6':
 print('退出系统')
 # ⼯作中，需要根据实际需求调优代码。
 # 1. ⽤户选择系统功能的代码需要循环使⽤，直到⽤户主动退出系统。
 # 2.
 # 如果⽤户输⼊1 - 6
 # 以外的数字，需要提示⽤户。
while True:
    # 1. 显示功能界⾯
    print_info()

    # 2. ⽤户选择功能
    user_num = input('请选择您需要的功能序号：')
    # 3. 根据⽤户选择，执⾏不同的功能
    if user_num == '1':
        print('添加学员')
        elif user_num == '2':
            print('删除学员')
        elif user_num == '3':
            print('修改学员信息')
        elif user_num == '4':
            print('查询学员信息')
        elif user_num == '5':
            print('显示所有学员信息')
        elif user_num == '6':
            print('退出系统')
        else:
            print('输⼊错误，请重新输⼊!!!')
# 1.3.4 定义不同功能的函数
# 所有功能函数都是操作学员信息，所有存储所有学员信息应该是⼀个全局变量，数据类型为列表。
info = []
# 1.3.4.1 添加学员
# 需求分析
# 1. 接收⽤户输⼊学员信息，并保存
# 2. 判断是否添加学员信息
# 2.1 如果学员姓名已经存在，则报错提示
# 2.2 如果学员姓名不存在，则准备空字典，将⽤户输⼊的数据追加到字典，再列表追加字典数据
# 3. 对应的if条件成⽴的位置调⽤该函数
def add_info():
    """ 添加学员 """
    # 接收⽤户输⼊学员信息
    new_id = input('请输⼊学号：')
    new_name = input('请输⼊姓名：')
    new_tel = input('请输⼊⼿机号：')

    # 声明info是全局变量
    global info
    # 检测⽤户输⼊的姓名是否存在，存在则报错提示
    for i in info:
        if new_name == i['name']:
            print('该⽤户已经存在！')
            return
    # 如果⽤户输⼊的姓名不存在，则添加该学员信息
    info_dict = {}

    # 将⽤户输⼊的数据追加到字典
    info_dict['id'] = new_id
    info_dict['name'] = new_name
    info_dict['tel'] = new_tel

    # 将这个学员的字典数据追加到列表
    info.append(info_dict)

    print(info)
# 1.3.4.2 删除学员
# 需求分析
# 按⽤户输⼊的学员姓名进⾏删除
# 1. ⽤户输⼊⽬标学员姓名
# 2. 检查这个学员是否存在
# 2.1 如果存在，则列表删除这个数据
# 2.2 如果不存在，则提示“该⽤户不存在”
# 3. 对应的if条件成⽴的位置调⽤该函数
# 删除学员
def del_info():
    """删除学员"""
    # 1. ⽤户输⼊要删除的学员的姓名
    del_name = input('请输⼊要删除的学员的姓名：')
    global info
    # 2. 判断学员是否存在:如果输⼊的姓名存在则删除，否则报错提示
    for i in info:
        if del_name == i['name']:
           info.remove(i)
           break
        else:
            print('该学员不存在')
        print(info)
# 1.3.4.3 修改学员信息
# 需求分析
# 1. ⽤户输⼊⽬标学员姓名
# 2. 检查这个学员是否存在
# 2.1 如果存在，则修改这位学员的信息，例如⼿机号
# 2.2 如果不存在，则报错
# 3. 对应的if条件成⽴的位置调⽤该函数
# 修改函数
def modify_info():
    """修改函数"""
    # 1. ⽤户输⼊要修改的学员的姓名
    modify_name = input('请输⼊要修改的学员的姓名：')
    global info
    # 2. 判断学员是否存在：如果输⼊的姓名存在则修改⼿机号，否则报错提示
    for i in info:
        if modify_name == i['name']:
            i['tel'] = input('请输⼊新的⼿机号：')
            break
        else:
            print('该学员不存在')

        print(info)
# 1.3.4.4 查询学员信息
# 需求分析
# 1. ⽤户输⼊⽬标学员姓名
# 2. 检查学员是否存在
# 2.1 如果存在，则显示这个学员的信息
# 2.2 如果不存在，则报错提示
# 3. 对应的if条件成⽴的位置调⽤该函数
# 查询学员
def search_info():
    """查询学员"""
    # 1. 输⼊要查找的学员姓名：
    search_name = input('请输⼊要查找的学员姓名：')
    global info
    # 2. 判断学员是否存在：如果输⼊的姓名存在则显示这位学员信息，否则报错提示
    for i in info:
        if search_name == i['name']:
            print('查找到的学员信息如下：----------')
            print(f"该学员的学号是{i['id']}, 姓名是{i['name']}, ⼿机号是{i['tel']}")
            break
        else:
            print('该学员不存在')
# 1.3.4.5 显示所有学员信息
# 需求分析
# 打印所有学员信息
# 显示所有学员信息
def print_all():
    """ 显示所有学员信息 """
    print('学号\t姓名\t⼿机号')
    for i in info:
        print(f'{i["id"]}\t{i["name"]}\t{i["tel"]}')
# 1.3.4.6 退出系统
# 在⽤户输⼊功能序号 6 的时候要退出系统，
    elif user_num == '6':
         exit_flag = input('确定要退出吗？yes or no')
         if exit_flag == 'yes':
             break

# 2.1 递归的应⽤场景
# 2.2 应⽤：3以内数字累加和
# 3 + 2 + 1
def sum_numbers(num):
    # 1.如果是1，直接返回1 -- 出⼝
    if num == 1:
        return 1
    # 2.如果不是1，重复执⾏累加并返回结果
    return num + sum_numbers(num-1)
sum_result = sum_numbers(3)
# 输出结果为6
print(sum_result)
# 3.3.2 lambda
fn1 = lambda a, b: a + b
print(fn1(1, 2))
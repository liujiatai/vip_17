#1.需求：有三个办公室，8位老师，8位老师随机分配到3个办公室
import random#导入random模块
#八位老师
teacher=[1,2,3,4,5,6,7,8]
#room_list
room_list=[[],[],[],]
#分配老师
for i in teacher:
    #三个教室
    room = random.randint(0, 2)
    #进教室
    room_list[room].append(i)
print(room_list)

#2求100以内能被3整除的数，并将作为列表输出
list1=[]
for n in range(1,101):
    if n%3==0:
        list1.append(n)
    else:
        continue
print(list1)

#3、列表[1,2,3,4,3,4,2,5,5,8,9,7],将此列表去重，得到一个唯一元素的列表
#不允许用强制类型转化
list2=[1,2,3,4,3,4,2,5,5,8,9,7]
list3=[]
for i in list2:
    if i not in list3:
        list3.append(i)
print(list3)

#4求斐波那契数列 1 1 2 3 5 8 13
#a1=1, a2=1, a3=a2+a1, a4=a3+a2,...
l=[]
n=0
for n in range(20):
    #第一个，第二个元素
    if n==0 or n==1:
        l.append(1)
    else:
        #第三个起
        l.append((l[n-1]+l[n-2]))
print(l)

#5.求100以内的质数（只能被1和它本身整除）
lista=[]
for i in range(2,101):
    for n in range(2,i):
        if i%n==0:
           break
    else:
        lista.append(i)
print(lista)

#6. 有一堆字符串“aabbbcddef”打印出不重复的字符串结果：cef
str1="aabbbcddef"
listb=[]#定义listb列表来承接筛选结果
for i in str1:
    #筛选出符合要求的元素
    if str1.count(i)==1:
        listb.append(i)
#输出结果，使用join方法来把得到的列表转换为字符串输出
print(','.join(listb))

#7.有一堆字符串，“welocme to super&Test”，打印出superTest，#不能查字符的索引
str2="welocme to super&Test"
#去掉字符&
list4=str2.split('&')
#剩余的部分组成新的字符串
str3=''.join(list4[0])+''.join(list4[1])
#按空格分割
list5=str3.split(' ',2)
#输出需要的字符串
print(''.join(list5[2]))

#8.有一堆字符串，“welocme to super&Test”，打印出tseT&repus ot ……全部单词原位置反转  #不允许用reverse
str4='welocme to super&Test'
str5=str4[::-1]
print(str5)

#9.有一堆字符串，“abcdef”，将收尾反转，结果：fedcba，不能使用现有的函数或方法，自己写算法实现
str6='abcdef'
#定义一个列表，将字符串元素放入列表
list6=list(str6)
#定义一个空列表
l=[]
#使用pop倒序遍历取元素
for i in range(len(str6)):
    m=list6.pop()
    #赋给新列表
    l.append(m)
#使用join将结果作为字符串输出
print(''.join(l))

#10.有一堆字符串，“aabbbcddef”，输出abcdef # 不允许用set
S='aabbbcddef'
#将S字符串转换为列表
listS=list(S)
#设置一个空列表
M=[]
#遍历，如果原列表里的元素不在新列表，则加入到新列表
for i in listS:
    if i not in M:
        M.append(i)
#使用join方法以字符串形式输出结果
print(''.join(M))



#1.字典修改键值对：字典名[key]= value,若没有，则新增
dict1 = {'name': 'Tom', 'age': 20, 'gender': '男'}
dict1['name']='sunke'
print(dict1)
dict1['id']= 112
print(dict1)
#2.删除键值对
dict2= {'name': 'sunke', 'age': 20, 'gender': '男', 'id': 112}
del dict2['gender']
print(dict2)
#3.查找
dict3= {'name': 'sunke', 'age': 20, 'gender': '男'}
print(dict3['name'])
print(dict3.get('id'))
print(dict3.get('age'))
print(dict3.keys())
print(dict3.values())
print(dict3.items())
#4.字典遍历
dict4 = {'name': 'Tom', 'age': 20, 'gender': '男'}
for key in dict4.keys():
    print(key)
for value in dict4.values():
    print(value)
for item in dict4.items():
    print(item)
for key,value in dict4.items():
    print(f'{key}={value}')

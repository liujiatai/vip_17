#1.创建集合使用{}或set()，但创建空集合只能使用set()，因为{}用来创建空字典
s1 = {10, 20, 30, 40, 50}
s2 = set()
s3= {}
print(type(s3))
print(type(s2))

#2.增加数据
#add()
s4={1,2,3}
s4.add(4)
print(s4)
s4.add(2)
print(s4)
#update()，追加的数据是序列
s5={2,3}
s5.update([100,200])
print(s5)
s5.update('cdefg')
print(s5)
#3.删除数据
#discard()，删除集合中的指定数据，如果数据不存在也不会报错
s6 = {10, 20}
s6.discard(10)
print(s6)
s6.discard(10)
print(s6)
#pop()，随机删除集合中的某个数据，并返回这个数据。
s7 = {10, 20, 30, 40, 50}
del_num = s7.pop()
print(del_num)
print(s7)
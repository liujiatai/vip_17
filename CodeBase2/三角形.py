#正三角形
n=5
for i in range(n+1):
    for j in range(0, (n+1) - i):
        print(end=" ")
    for k in range((n+1) - i, (n+1)):
        print("*", end=" ")

    print("")
#倒等边三角形
n=5
for i in range(n):
    for j in range(0, i):
        print(end=" ")
    for k in range(i, n):
        print("*", end=" ")

    print("")
#左上角实心三角形
n=5
for i in range (0,n+1):
    for j in range (0,n-i):
         print("*",end=" ")
    print()
#左上角空心三角形
n=5
for i in range (0,n+1):
    for j in range (0,n-i):
      if (i==0) or (j==0) or i+j+1==n:
         print("*",end=" ")
      else:
          print(" ",end=" ")
    print()

#右上角实心三角形
n=5
for i in range(n):
    for j in range(0,i):
        print(" ", end=" ")
    for k in range(i,n):
        print("*", end=" ")
    print("")
#右上角空心三角形
n=5
for i in range (1,n+1):
    for j in range (1,n+1):
        if i==1 or j==n or i==j:
            print("*",end=" ")
        else:
            print(" ",end=" ")
    print()


#1.+ 合并
# 1. 字符串
str1 = 'aa'
str2 = 'bb'
str3 = str1 + str2
print(str3)
# 2. 列表
list1 = [1, 2]
list2 = [3, 4]
list3 = list1 + list2
print(list3)
# 3. 元组
t1 = (1, 2)
t2 = (3, 4)
t3 = t1 + t2
print(t3)

#2.*复制
# 1. 字符串
print('-' * 10)
# 2. 列表
list1 = ['hello']
print(list1 * 4)
# 3. 元组
t1 = ('world',)
print(t1 * 4)

#3.Max和Min
#Max
# 1. 字符串
str1 = 'abcdefg'
print(max(str1))
# 2. 列表
list1 = [10, 20, 30, 40]
print(max(list1))
#Min
# 1. 字符串
str1 = 'abcdefg'
print(min(str1))
# 2. 列表
list1 = [10, 20, 30, 40]
print(min(list1))

#4.enumerate()
list1 = ['a', 'b', 'c', 'd', 'e']
for i in enumerate(list1):
 print(i)
for index, char in enumerate(list1, start=1):
 print(f'下标是{index}, 对应的字符是{char}')
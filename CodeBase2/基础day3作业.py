#1.使用列表推导式生成能被5整除的数（100以内）
list1=[i for i in range(1,101) if i%5==0]
print(list1)
#2.有两个列表，一个是学生姓名，一个是学生的年龄，生成一个字典，key为姓名，value为年龄
name=['lili','lilei','xiaowang','xiaoming','xiaohong']
age=[10,12,15,17,19]
#1使用lambda
student=dict(map(lambda x,y:(x,y),name,age))
print(student)
#2字典推导式
student_dict={name[i]:age[i] for i in range(len(name))}
print(student_dict)

#3.开发一个注册系统，
# 页面：
# [{name:xxx,age:xxx},{name:xxx,age:xxx},{name:xxx,age:xxx}]
# ----------------
# *   1-新增用户
# *   2-查询用户
# *   3-删除用户
# ----------------
# 功能1：新增学生信息（姓名和年龄）通过键盘，如果学生已经存在提示用户已存在
# 功能2：查询学生信息
# 功能3：删除学生信息

#初始库
user_list=[{'name':'小明','age':19},{'name':'小红','age':18},{'name':'小花','age':18}]
#新增用户
def user_add(name,age):
    '''
    新增用户信息，若用户已存在，则不新增，并提示
    :param name:姓名
    :param age:年龄
    :return:none
    '''
    #遍历判断用户是否已存在，若已存在则提示并跳出
    for i in user_list:
        if i['name']==name:
            print('用户已存在')
            break
    else:
            infor={'name':name,'age':age}
            user_list.append(infor)
            print('添加新用户成功--->',infor)
#查询所有用户信息
def print_all():
    '''
    查询所有学生信息
    :return:none
    '''
    for i in user_list:
        print('学生信息',i)
#查询单个学生信息
def print_user(name):
    '''
    查询某个学生信息，存在则打印，不存在则给出提示
    :param name:姓名
    :return:none
    '''
    #遍历查询学生信息，取到则打印信息并退出循环
    for i in range(len(user_list)):
        if name==user_list[i]['name']:
            print('学生信息是：',user_list[i])
            break
        #若取不到则给出提示
    else:
            print('您查询的用户信息不存在')
#删除某个学生的信息
def  user_del(name):
    '''
    删除某个学生信息
    :param name:姓名
    :return:none
    '''
    #遍历学生信息是否存在，存在则删除退出循环
    for i in range(len(user_list)):
        if name==user_list[i]['name']:
            print('删除学生信息',user_list.pop(i))
            break
    else:
            print('您要删除的用户信息不存在')

#入口函数
def run():
    '''
    学生信息管理系统入口函数
    :return:
    '''
    #循环首页以供用户做出选择
    while True:
        #提示
        print('-----------------------')
        print('1.新增用户')
        print('2.查看所有学生信息')
        print('3.查找学生信息')
        print('4.删除学生信息')
        print('5.退出系统')
        print('-----------------------')
        #进行功能选择
        n=input('请选择操作项：')
        #调用添加新用户
        if n=='1':
            print('调用新增用户函数')
            name=input('请输入学生姓名：')
            age=int(input('请输入学生年龄'))
            user_add(name,age)
        #调用查询所有用户信息
        elif n=='2':
            print('调用查询所有学生信息')
            print_all()
        #调用查询学生信息
        elif n=='3':
            print('调用查找学生信息')
            name=input('请输入学生姓名：')
            print_user(name)
        #调用删除学生信息
        elif n=='4':
            print('调用删除学生信息函数')
            name=input('请输入要删除的学生姓名：')
            user_del(name)
        #退出系统
        elif n=='5':
            print('退出系统')
            break
        #输入其他情况非法
        else:
            print('输入数据非法')
#执行入口函数
run()

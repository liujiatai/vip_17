#1.函数参数
def add_num2(a,b):
    result= a+b
    print(result)
add_num2(3,5)
#2.函数返回值
def buy():
    return '酒'
goods = buy()
print(goods)
#2.1函数返回应用
def sum_num(a,b):
    return a+b
res=sum_num(1,8)
print(res)
#3.函数嵌套使用
def testB():
    print('----testB start----')
    print('testB')
    print('----testB end----')
def testA():
    print('----testA start----')
    testB()
    print('----testA end----')
testA()

#4.修改函数全局变量，使用global
a=100
def test1():
    print(a)
def test2():
    global a
    a=200
    print(a)
test1()
test2()
print(f'全局变量a={a}')

#5.函数返回值作为参数传递
def test1():
    return 10
def test2(a):
    print(a)
res= test1()
test2(res)

#6.函数多个返回值
def return_num():
    return 1,2,3,4,5
res=return_num()
print(res)

#7.函数位置参数
def user_info(name,age,gender):
    print(f'您的名字是{name}，年龄是{age}，性别是{gender}')
user_info('小明',17,'男')
#7.1关键字参数
def user_info(name, age, gender):
 print(f'您的名字是{name}, 年龄是{age}, 性别是{gender}')
user_info('Rose', age=20, gender='⼥')
user_info('⼩明', gender='男', age=16)
#7.2缺省参数
def user_info(name, age, gender='男'):
 print(f'您的名字是{name}, 年龄是{age}, 性别是{gender}')
user_info('TOM', 20)
user_info('Rose', 18, '⼥')

#8.函数不定长参数
#8.1包裹位置传递
def user_info(*args):
 print(args)
user_info('TOM') # ('TOM',)
user_info('TOM', 18) # ('TOM', 18)
list1 = [1,2,3] #提前定义列表
user_info(*list1) #传入的时候必须带*
#8.2包裹关键字传递
def user_info(**kwargs):
 print(kwargs)
user_info(name='TOM', age=18, id=110) # {'name': 'TOM', 'age': 18, 'id': 110}
dict1= {'name': 'TOM', 'age': 18, 'id': 110} # 提前定义字典
user_info(**dict1) # 传入字典必须带**

#9.拆包
#9.1拆包，元组
def return_num():
 return 100, 200
num1, num2 = return_num()
print(num1)
print(num2)
#9.2拆包，字典
dict1 = {'name': 'TOM', 'age': 18}
a, b = dict1
# 对字典进⾏拆包，取出来的是字典的key
print(a)
print(b)
print(dict1[a])
print(dict1[b])
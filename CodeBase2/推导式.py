#1.列表推导式
#创建一个0-10的列表
#while循环
#准备一个空列表
list1=[]
#写循环，依次将数字追加到空列表中
i=0
while i<10:
    list1.append(i)
    i += 1
print(list1)
#for循环
list2=[]
for i in range(10):
    list2.append(i)
print(list2)
#列表推导式
list3 = [i for i in range(10)]
print(list3)

#2.带if的列表推导式
#创建0-10的偶数列表
list1= [i for i in range(10) if i%2 ==0]
print(list1)

#3.多个for循环实现列表推导式
list1= [(i,j) for i in range(1,3) for j in range(3)]
print(list1)

#4.字典推导式
#4.1创建⼀个字典：字典key是1-5数字，value是这个数字的2次⽅。
dict1 = {i:i**2 for i in range(1,6)}
print(dict1)
#4.2将两个列表合并为一个字典
list1 = ['name', 'age', 'gender']
list2 = ['Tom', 20, 'man']
dict1 = {list1[i]:list2[i] for i in range(len(list1))}
print(dict1)
#4.3提取字典中⽬标数据
counts = {'MBP': 268, 'HP': 125, 'DELL': 201, 'Lenovo': 199, 'acer': 99}
#提取上述电脑数量大于等于200的字典数据
count1 = {key:value for key,value in counts.items() if value>=200}
print(count1)

#5.集合推导式
#创建⼀个集合，数据为下⽅列表的2次⽅。
list1 = [1, 1, 2]
set1= {i**2 for i in list1}
print(set1)
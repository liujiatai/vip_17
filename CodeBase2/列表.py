#1.列表增加append
name_list = ['Tom', 'Lily', 'Rose']
name_list.append('xiaoming')
print(name_list)

#2.sort排序
num_list = [1, 5, 2, 3, 6, 8]
num_list.sort()
print(num_list)

#3.while遍历列表
name_list = ['Tom', 'Lily', 'Rose']
i=0
while i < len(name_list):
    print(name_list[i])
    i += 1
#4.for遍历列表
name_list = ['Tom', 'Lily', 'Rose']

for n in range(len(name_list)):
    print(name_list[n])
#5.insert():指定位置增加数据
list1= ['lili','lilei']
list1.insert(0,'xiaozhang')
print(list1)

#6.del删除指定数据
list2= ['lili','lilei']
del list2[0]
print(list2)

#7.pop()默认删除最后一个数据
list3=['Tom', 'Lily', 'Rose']
list3.pop()
print(list3)

#8.remove()移除列表中某个数据的第一个匹配项
list4=['Tom', 'Lily', 'Rose', 'Lily']
list4.remove('Lily')
print(list4)